package np.com.ideabreed.servicesexample;

import android.content.Context;
import android.os.AsyncTask;
import android.widget.Toast;

public class AsyncDemo extends AsyncTask<String, String, String> {

    private Context context;

    public AsyncDemo(Context context){
        this.context = context;
    }

    @Override
    protected String doInBackground(String... strings){
        return strings[0];
    }

    @Override
    protected void onPreExecute(){
        super.onPreExecute();

        Toast.makeText(context, "On PreExecute Call", Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onPostExecute(String s){
        super.onPostExecute(s);

        Toast.makeText(context, "" + s, Toast.LENGTH_SHORT).show();
    }
}
