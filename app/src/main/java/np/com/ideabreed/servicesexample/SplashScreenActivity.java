package np.com.ideabreed.servicesexample;

import android.content.Intent;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

public class SplashScreenActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);

        Thread thread = new Thread() {
            @Override
            public void run(){
                try {
                    sleep(5000);
                    startActivity(new Intent(SplashScreenActivity.this, MainActivity.class));
                } catch(InterruptedException e) {
                    e.printStackTrace();
                }
            }
        };
        thread.start();


        //        final Handler handler = new Handler();
        //        final Runnable runnable = new Runnable() {
        //
        //            int i = 0;
        //
        //            @Override
        //            public void run(){
        //
        //                Toast.makeText(SplashScreenActivity.this, "Run" + i, Toast
        //                .LENGTH_SHORT).show();
        //                i++;
        //                if(i >= 6) {
        //                    i = 0;
        //                }
        //                handler.postDelayed(this, 2000);
        //            }
        //        };
        //
        //        handler.postDelayed(runnable, 5000);


    }
}
